#!/usr/bin/env python

# Heavily inspired by
# https://github.com/jpetazzo/orchestration-workshop/blob/master/build-tag-push.py

import sys
import subprocess
import yaml

# build-stage.py <project_name> <version> <registry_url>
project_name = sys.argv[1]
version = sys.argv[2]
registry_url = sys.argv[3]

input_file = 'docker-compose.yml'
output_file = 'docker-compose-staging.yml'

subprocess.check_call(['sudo', 'docker-compose', '-f', input_file,
                       '-p', project_name, 'build'])

stack = yaml.load(open(input_file))

for service_name, service in stack.items():
    if 'build' in service:
        compose_image = '{}_{}'.format(project_name, service_name)
        registry_image = '{}/{}_{}:{}'.format(registry_url, project_name,
                                              service_name, version)
        subprocess.check_call(['sudo', 'docker', 'tag', compose_image, registry_image])
        subprocess.check_call(['sudo', 'docker', 'push', registry_image])
        del service['build']
        service['image'] = registry_image

with open(output_file, 'w') as f:
    yaml.safe_dump(stack, f, default_flow_style=False)

# Add additional environment variables for HAProxy Setup
